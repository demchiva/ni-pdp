#include <iostream>
#include <fstream>
#include <utility>
#include <vector>
#include <sstream>
#include <chrono>
#include <cstdlib>
#include <math.h>
#include <climits>

using namespace std;

enum SetCompareResult {
    SAME, NOT_SAME, NOT_DEFINED
};

class Combination {
    public:
        int size;
        int maxOneCount;
        int maxZeroCount;
        int currentOneCount;
        vector<bool> points;

        Combination() {}

        Combination(int n, int na) {
            currentOneCount = 0;
            size = n;
            maxOneCount = na;
            maxZeroCount = n - na;
        }

        /**
         * The method create the new combination.
         */
        Combination createNext(bool value) {
            Combination n = Combination(*this);
            n.addValue(value);
            return n;
        }

        /**
         * The method get info if the current combination is valid result.
         */
        bool isValid() {
            return points.size() == size && maxOneCount == currentOneCount;
        }

        /**
         * The method get value if the vertexes in one set or not.
         */
        SetCompareResult isDifferentSets(int v1, int v2) {
            if (v1 >= points.size() || v2 >= points.size()) {
                return SetCompareResult::NOT_DEFINED;
            }

            return points.at(v1) != points.at(v2) ? SetCompareResult::NOT_SAME : SetCompareResult::SAME;
        }

        /**
         * The method say if it is not possible to continue in solve finding.
         */
        bool shouldStop() {
            return (points.size() - currentOneCount) > maxZeroCount || currentOneCount > maxOneCount;
        }

        /**
         * The method print the combination.
         */
        void print() {
            cout << "-----------------------" << endl;
            int cutEdgesCount = 0;
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < i; j++) {
                    if (isDifferentSets(i, j) == SetCompareResult::NOT_SAME) {
                        cout << i << " " << j << "   Not same" << "\n";
                        cutEdgesCount += 1;
                    } else {
                        cout << i << " " << j << "   Same" << "\n";
                    }
                }
            }
            cout << "Edges cut count: " << cutEdgesCount << endl;
            cout << "-----------------------" << endl;     
        }

        bool equals(Combination & other) {
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++) {
                    if (isDifferentSets(i, j) != other.isDifferentSets(i, j)) {
                        return false;
                    }
                }
            }

            return true;
        }

    private:
        /**
         * The method add the value to current combination.
         */
        void addValue(bool value) {
            points.push_back(value);
            if (value) {
                currentOneCount += 1;
            }            
        }
};

/**
 *  The struct represents the Edge of Graph.
 */
struct E {
    int v1;
    int v2;
    int cost;

    E(int v1, int v2, int cost) : v1(v1), v2(v2), cost(cost) {}
};


class Graph {
    public:
        int bestCost;
        int callCount;
        int solveCount;
        vector<Combination> bestCombs;

        int n;
        vector<E> edges;
        
        Graph(int n) {
            this->n = n;
            bestCost = INT_MAX;
            callCount = 0;
            solveCount = 0;
        }

        /**
         *  The method returns true if comb is the same as one of best combinations.
         */
        bool equlasOfSomeCombination(Combination & comb) {
            for (Combination & bc : bestCombs) {
                if (comb.equals(bc)) {
                    return true;
                }
            }

            return false;
        }

        /**
         * The method calculates the new cost.
         */
        int calculateCost(Combination & comb, vector<E> & edges) {
            int newCost = 0;

            for (E e : edges) {
                if (comb.isDifferentSets(e.v1, e.v2) == SetCompareResult::NOT_SAME) {
                    newCost += e.cost;
                }
            }

            return newCost;
        }

        /**
         * The method calculates the lower estimation of cost.
         */
        int calculateLowerCostEstimation(Combination & comb) {
            int lowerCostEstimation = 0;

            for (int i = comb.points.size(); i < n; i++) {
                int right = 0;
                int left = 0;

                for (E & e : edges) {
                    // Simulated v1 in 0 set.
                    if ((e.v1 == i) && e.v2 < comb.points.size() && comb.points.at(e.v2)) {
                        left += e.cost;
                    }

                    // Simulated v1 in 1 set.
                    if ((e.v1 == i) && e.v2 < comb.points.size() && !comb.points.at(e.v2)) {
                        right += e.cost;
                    }
                }

                lowerCostEstimation += min(left, right);
            }

            return lowerCostEstimation;
        }

        /**
         * The method solves the min cut graph problem using B&B.
         */
        void solve(Combination & comb) {
            callCount += 1;

            if (comb.shouldStop()) {
                return;
            }

            int newCost = calculateCost(comb, edges);

            if (newCost > bestCost) {
                return;
            }

            if (comb.isValid()) {
                if (newCost == bestCost) {
                    if (!equlasOfSomeCombination(comb)) {
                        bestCombs.push_back(comb);
                    }
                } else {
                    bestCombs.clear();
                    bestCombs.push_back(comb);
                    bestCost = newCost;
                }

                return;
            }

            if (newCost + calculateLowerCostEstimation(comb) > bestCost) {
                return;
            }
            
            Combination leftComb = comb.createNext(false);
            Combination rightComb = comb.createNext(true);

            solve(leftComb); // left branch
            solve(rightComb); // right branch
        }

        /**
         * The method print the best combinations.
         */
        void print() {
            for (Combination & bc : bestCombs) {
                bc.print();
            }
        }
};


/**
 *  The class reads the given file and creates Graph instance.
 */
class FileReader {
    public:
        static Graph createGraph(string fileName, int a) {
            string line;
            ifstream instance(fileName);
            int n = 0;
            stringstream ss;
            int idCounter = 0;

            if (instance.is_open()) {
                getline(instance, line);
                ss << line;
                ss >> n;
                Graph g = Graph(n);

                while (getline(instance, line)) {
                    stringstream ss2;
                    ss2 << line;

                    int neighbourId = 0;

                    while (!ss2.eof() && neighbourId < idCounter + 1) {
                        int cost;
                        ss2 >> cost;

                        if (cost != 0) {
                            g.edges.push_back(E(idCounter, neighbourId, cost));
                        }

                        neighbourId++;
                    }

                    idCounter++;
                }

                instance.close();
                return g;
            }
        }
};

/**
 * The function fill the instances for solve.
 */
void fillInstances(vector<pair<string, int>> & instances) {
    // instances.push_back(make_pair("graf_10_5", 5));
    // instances.push_back(make_pair("graf_10_6b", 5));
    // instances.push_back(make_pair("graf_10_7", 5));
    instances.push_back(make_pair("graf_15_14", 5));
    // instances.push_back(make_pair("graf_20_7", 7));
    // instances.push_back(make_pair("graf_20_7", 10));
    // instances.push_back(make_pair("graf_20_12", 10));
    instances.push_back(make_pair("graf_20_17", 10));
    // instances.push_back(make_pair("graf_30_10", 10));
    // instances.push_back(make_pair("graf_30_10", 15));
    // instances.push_back(make_pair("graf_30_20", 15));
    // instances.push_back(make_pair("graf_40_8", 15));
    // instances.push_back(make_pair("graf_40_8", 20));
    // instances.push_back(make_pair("graf_40_15", 20));
    // instances.push_back(make_pair("graf_40_25", 20));
}

/**
 * The main function of the program.
 */
int main() {
    vector<pair<string, int>> instances;
    fillInstances(instances);

    for (pair<string, int> instance : instances) {
        int a = instance.second;
        Graph g = FileReader::createGraph("./graf_mhr/" + instance.first + ".txt", a);
        Combination first = Combination(g.n, a);
        g.solve(first);
        cout << "Best result: " << g.bestCost << "    Solve count: " <<  g.bestCombs.size() << "   Call count: " << g.callCount << endl;
        g.print();
    }

    return 0;
}